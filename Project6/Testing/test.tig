/* Test Type Checking */
let
    var N := 8
    var S := "soup"
    var A := 9
    type intArray = array of int
    type intList = { head: int, tail: intlist }
    var list : intList := nil

    /* Type checking on SubscriptVar like: lvalue[exp] */
    /* lvalue is not an array type */
    var badLvalue := badArray[ N ] of 0 /* (test case 1) */

    /*  exp is not an integer */
     var badExpr := intArray[ S ] of 0 /* (test case 2) */

     /* Type checking on OpExp like: exp1 operator exp2 */
     /* If operator is +, -, *, or / */
     /* exp1 is not an integer */
     var plus := "2" + 2    /* (test case 3) */
     var minus := "2" - 2
     var times := "2" * 2
     var div := "2" / 2

     /* exp2 is not an integer */
     var plus2 := 2+ "2"    /* (test case 4) */
     var minus2 := 2 - "2"
     var times2 := 2 * "2"
     var div2 := 2 / "2"

     /* Type checking on ArrayExp like: array_typename [exp1] of exp2 */
     /* array_typename is not defined */
     var x := badlist[N] of 0           /* (test case 23) */
     /* array_typename is defined, but not an array type */
     var y := intList[N] of 0           /* (test case 24) */
     /* exp1 is not an integer expression */
     var z := intArray[S] of 0          /* (test case 25) */
     /* Type of exp2 doesn’t match the definition of array_typename */
     var w := intArray[N] of "0"        /* (test case 26) */

     /* Type checking on VarDec like: var vname : TypeName := exp */
     /* vname already exists */
     var A : int := 10           /* (test case 27) */
     /* Type of exp doesn’t match TypeName */
     var bim : int := "int"      /* (test case 28) */
     /* If TypeName is omitted, and exp is NIL. */
     var bat := nil            /* (test case 29) */

     /* If operator is =, or <> */
     /* Types of exp1 and exp2 doesn’t match */
     function foo( listyList: intList ) = (
         if "2" = 2 then S := "Test Failed" /*  (test case 5) */
         if "2" <> 2 then S := "Test Failed"
         /* Exp1 is NIL but exp2 is not an array or record */
         if nil = N then S := "Test Failed"     /* (test case 6) */
         if nil <> N then S := "Test Failed"
         /* Both exp1 and exp2 are NIL */
         if nil = nil then S := "Test Failed"   /* (test case 7) */
         if nil <> nil then S := "Test Failed"
         /* If operator is <, <=, >, or >= */
         /* One operand is neither INT nor STRING */
         if listyList < 2 then S := "Test Failed"   /* (test case 8) */
         if listyList <= 2 then S := "Test Failed"
         if listyList > 2 then S := "Test Failed"
         if listyList >= 2 then S := "Test Failed"
         /* One operand is INT, and the other one is STRING */
         if "S" < 2 then S := "Test Failed"     /* (test case 9) */
         if "S" <= 2 then S := "Test Failed"
         if "S" > 2 then S := "Test Failed"
         if "S" >= 2 then S := "Test Failed"
     )

     function funkyTown( number: int ) : int = (
         /* Type checking on IfExp like: if exp1 then exp2 else exp3 */
         /* exp1 is not an integer */
         if S then S := "Test Failed"       /* (test case 16) */
         /*  if there is no else-clause, exp2 is not VOID type */
         if 1 < 2 then 1+2+3                /* (test case 17) */
         /* if there is else-clause, types of exp2 and exp3 don’t match */
         if 1 < 2 then "String" else 1+2    /* (test case 18) */
         if number = 0 then 0
         else number + 2
     )
 in
     foo (list);
     /* Type checking on CallExp like: fname(exp1, exp2, …, expn) */
     /* fname is not defined */
     badCall(N);     /* (test case 10) */
     /* fname is defined as a variable */
     N(S);           /* (test case 11) */
     /* too many arguments */
     foo(list, N);   /* (test case 12) */
     /* less arguments than required */
     foo();          /* (test case 13) */
     /* one expression has wrong data type */
     foo(N);         /* (test case 14) */

     /* Type checking on AssignExp like: lvalue := exp */
     /* Types of lvalue and exp don’t match */
     A := "9"       /*  (test case 15) */

     /* Type checking on WhileExp */
     /* The body of the loop is not VOID type */
     while A = 9 do A := 0;             /* (test case 19) */

     /* Type checking on ForExp like: for varname := exp1 to exp2 do exp3 */
     /* exp1 is not an integer expression */
     for i := "S" to A - 1 do foo();         /* (test case 20) */
     /* exp2 is not an integer expression */
     for i := 0 to S - 1 do foo(i);           /* (test case 21) */
     /* exp3 is not VOID type */
     for i := 0 to A - 1 do funkyTown(i)     /* (test case 22) */
end
