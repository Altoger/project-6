/*
    PROGRAMMER: L. K. Silkeutsabay
    PROGRAM #: 6
    DUE DATE: 11/09/2020
    INSTRUCTOR: Dr. Zhijiang Dong
*/
#include "TypeChecking.h"
using namespace symbol;
namespace semantics
{

	const string		TypeChecking::breakSign = "breaksign";
	//insert a variable into the var/function symbol table
	void TypeChecking::insertVar(string name, symbol::SymTabEntry entry)
	{
		string			msg;
		stringstream	ss(msg);
		if (env.getVarEnv()->localContains(name))
		{
			symbol::SymTabEntry		old = env.getVarEnv()->lookup(name);
			ss << "variable " << name << " is already defined at line " << old.node->getLineno();
			error(entry.node, ss.str());
		}
		else
			env.getVarEnv()->insert(name, entry);
	}
	//insert a function into the var/function symbol table
	void TypeChecking::insertFunc(string name, symbol::SymTabEntry entry)
	{
		string			msg;
		stringstream	ss(msg);
		if (env.getVarEnv()->localContains(name))
		{
			symbol::SymTabEntry		old = env.getVarEnv()->lookup(name);
			ss << "function " << name << " is already defined at line " << old.node->getLineno();
			error(entry.node, ss.str());
		}
		else
			env.getVarEnv()->insert(name, entry);
	}
	//insert a type into the type symbol table
	void TypeChecking::insertType(string name, symbol::SymTabEntry entry)
	{
		string			msg;
		stringstream	ss(msg);
		if (env.getTypeEnv()->localContains(name))
		{
			symbol::SymTabEntry		old = env.getTypeEnv()->lookup(name);
			ss << "variable " << name << " is already defined at line " << old.node->getLineno();
			error(entry.node, ss.str());
		}
		else
			env.getTypeEnv()->insert(name, entry);
	}
	const types::Type* TypeChecking::visit(const Absyn *v)
	{
		if (dynamic_cast<const Exp *>(v) != NULL)
			return visit(dynamic_cast<const Exp *>(v));
		else if (dynamic_cast<const Var *>(v) != NULL)
			return visit(dynamic_cast<const Var *>(v));
		else if (dynamic_cast<const Dec *>(v) != NULL)
			return visit(dynamic_cast<const Dec *>(v));
		else
			throw runtime_error("invalid node");
	}
	const types::Type* TypeChecking::visit(const Exp *e)
	{
		if (dynamic_cast<const OpExp*>(e) != NULL)			return visit((const OpExp*)e);
		else if (dynamic_cast<const VarExp*>(e) != NULL)	return visit((const VarExp*)e);
		else if (dynamic_cast<const NilExp*>(e) != NULL)	return visit((const NilExp*)e);
		else if (dynamic_cast<const IntExp*>(e) != NULL)	return visit((const IntExp*)e);
		else if (dynamic_cast<const StringExp*>(e) != NULL) return visit((const StringExp*)e);
		else if (dynamic_cast<const CallExp*>(e) != NULL)	return visit((const CallExp*)e);
		//		else if (dynamic_cast<const RecordExp*>(e) != NULL) return visit((const RecordExp*)e);
		else if (dynamic_cast<const SeqExp*>(e) != NULL)	return visit((const SeqExp*)e);
		else if (dynamic_cast<const AssignExp*>(e) != NULL) return visit((const AssignExp*)e);
		else if (dynamic_cast<const IfExp*>(e) != NULL)		return visit((const IfExp*)e);
		else if (dynamic_cast<const WhileExp*>(e) != NULL)	return visit((const WhileExp*)e);
		else if (dynamic_cast<const ForExp*>(e) != NULL)	return visit((const ForExp*)e);
		else if (dynamic_cast<const BreakExp*>(e) != NULL)	return visit((const BreakExp*)e);
		else if (dynamic_cast<const LetExp*>(e) != NULL)	return visit((const LetExp*)e);
		else if (dynamic_cast<const ArrayExp*>(e) != NULL)	return visit((const ArrayExp*)e);
		else throw new runtime_error("ExpType.visit(Exp*)");
	}
	const types::Type* TypeChecking::visit(const Var *v)
	{
		if (dynamic_cast<const SimpleVar *>(v) != NULL)			return visit((const SimpleVar *)v);
		//		else if (dynamic_cast<const FieldVar *>(v) != NULL)		return visit((const FieldVar *) v);
		else if (dynamic_cast<const SubscriptVar *>(v) != NULL) return visit((const SubscriptVar *)v);
		else throw new runtime_error("ExpType.visit(Var*)");
	}
	const types::Type* TypeChecking::visit(const Ty *t)
	{
		if (dynamic_cast<const NameTy *>(t) != NULL)			return visit((const NameTy *)t);
		else if (dynamic_cast<const ArrayTy *>(t) != NULL)		return visit((const ArrayTy *)t);
		//		else if (dynamic_cast<const RecordTy *>(t) != NULL)		return visit((const RecordTy *)t);
		else throw new runtime_error("ExpType.visit(Ty*)");
	}
	const types::Type* TypeChecking::visit(const Dec *d)
	{
		if (dynamic_cast<const TypeDec *>(d) != NULL)			return visit((const TypeDec *)d);
		else if (dynamic_cast<const VarDec *>(d) != NULL)		return visit((const VarDec *)d);
		//		else if (dynamic_cast<const FunctionDec *>(d) != NULL)	return visit((const FunctionDec *)d);
		else throw new runtime_error("ExpType.visit(Dec*)");
	}
	const types::Type* TypeChecking::visit(const SimpleVar *v)
	{
		if (!(env.getVarEnv()->contains(v->getName())))
		{
			error(v, "undefined variable");
			//undeclared variables is treated as INT variable
			insertVar(v->getName(), SymTabEntry(env.getVarEnv()->getLevel(),
				new types::INT(),
				v));
			return new types::INT();
		}
		else
		{
			const types::Type*	t = env.getVarEnv()->lookup(v->getName()).info->actual();
			if (dynamic_cast<const types::FUNCTION *>(t) != NULL)
			{
				error(v, "function with the same name exists");
				//undeclared variables is treated as INT variable
				return new types::INT();
			}
			return t;
		}
	}
	/*	const types::Type* TypeChecking::visit(const FieldVar *v)
	{
	//add your implementation here
	//syntax: lvalue.fieldname
	Algorithm:
	1.	Perform type checking on lvalue, and get its data type (say t)
	2.	if t is not a record type
	report an error
	return INT
	else
	3.		cast t to RECORD *;
	4.		For each filed in the RECORD definition
	if the fieldname is the one we are looking for
	return the type of current field
	5.		report an error for non-existing field
	6.		return INT.
	}
	*/
	const types::Type* TypeChecking::visit(const SubscriptVar *v)
	{
		//syntax: lvalue[index_exp]
		//Perform type checking on lvalue, and get its data type (say t)
		const types::Type* var_type = visit(v->getVar());
		//Perform type checking on index_exp, and get its data type (say te)
		const types::Type* index_type = visit(v->getIndex());
		const types::ARRAY* potential_array = dynamic_cast<const types::ARRAY *>(var_type);
		//if t is not ARRAY type
		if (potential_array != NULL) {
			//report an error
			error(v, "lvalue must be an Array type.");
		}
		//if te is not INT
		if (dynamic_cast<const types::INT *>(index_type) != NULL) {
			//report an error
			error(v, "index_exp must be a Int type.");
		}
		//if t is not ARRAY
		if (potential_array != NULL) {
			//return INT
			return new types::INT();
		}
		else {
			//return the type of array element which can be found at((ARRAY *)t)
			return var_type->actual();
		}
	}
	const types::Type* TypeChecking::visit(const OpExp *e)
	{
		//syntax: left_exp Operator right_exp
		//1.	Perform type checking on left_exp, and get its data type (say lt)
		const types::Type* lt = visit(e->getLeft());
		//2.	Perform type checking on right_exp, and get its data type (say rt)
		const types::Type* rt = visit(e->getRight());
		OpExp::OpType op = e->getOper();
		//3.	if Operator is one of +, -, *, /
		if (op == OpExp::OpType::PLUS || op == OpExp::OpType::MINUS || op == OpExp::OpType::MUL || op == OpExp::OpType::DIV) {
			//if lt is not an INT, report an error
			if (dynamic_cast<const types::INT *>(lt) != NULL) {
				error(e, "left expression has to be type INT");
			}
			//if rt is not an INT, report an error
			if (dynamic_cast<const types::INT *>(rt) != NULL) {
				error(e, "right expression has to be type INT");
			}
			//return INT
			return new types::INT();
		}
		//4.	else if Operator is one of >, >=, <, <=
		if (op == OpExp::OpType::GT || op == OpExp::OpType::GE || op == OpExp::OpType::LT || op == OpExp::OpType::LE) {
			//if lt is not an INT/STRING, report an error
			if (dynamic_cast<const types::INT *>(lt) != NULL || dynamic_cast<const types::STRING*>(lt) != NULL) {
				error(e, "left expression has to be type of INT or STRING");
			}
			//if rt is not an INT/STRING, report an error
			if (dynamic_cast<const types::INT *>(rt) != NULL || dynamic_cast<const types::STRING*>(rt) != NULL) {
				error(e, "right expression has to be type of INT or STRING");
			}
			//if lt and rt are not compatible
			if (!lt->coerceTo(rt) || !rt->coerceTo(lt)) {
				//report an error
				error(e, "Left end right expressions are incompatible");
			}
			//return INT;
			return new types::INT();
		}
		//else //i.e. =, <>
		//if lt is not an INT/STRING/ARRAY/RECORD/NIL, report an error
		if (dynamic_cast<const types::INT *>(lt) != NULL || dynamic_cast<const types::STRING *>(lt) != NULL || dynamic_cast<const types::ARRAY *>(lt) != NULL ||
			dynamic_cast<const types::RECORD *>(lt) != NULL || dynamic_cast<const types::NIL *>(lt) != NULL) {
			error(e, "Left expression has to be of type: INT, STRING, ARRAY. RECORD or NIL");
		}
		//if rt is not an INT/STRING/ARRAY/RECORD/NIL, report an error
		if (dynamic_cast<const types::INT *>(rt) != NULL || dynamic_cast<const types::STRING *>(rt) != NULL || dynamic_cast<const types::ARRAY *>(rt) != NULL ||
			dynamic_cast<const types::RECORD *>(rt) != NULL || dynamic_cast<const types::NIL *>(rt) != NULL) {
			error(e, "Right expression has to be of type: INT, STRING, ARRAY. RECORD or NIL");
		}
		//if lt and rt are not compatible
		if (!lt->coerceTo(rt) || !rt->coerceTo(lt)) {
			//report an error
			error(e, "Left end right expressions are incompatible");
		}
		//if both lt and rt are NIL
		if (lt == NULL && rt == NULL) {
			error(e, "Both left and right expressions cant be of a type NULL");
		}
		//return INT
		return new types::INT();
	}
	const types::Type* TypeChecking::visit(const VarExp *e)
	{
		const types::Type*		t = visit(e->getVar());
		return t->actual();
	}
	const types::Type* TypeChecking::visit(const NilExp *e)
	{
		return new types::NIL();
	}
	const types::Type* TypeChecking::visit(const IntExp *e)
	{
		return new types::INT();
	}
	const types::Type* TypeChecking::visit(const StringExp *e)
	{
		return new types::STRING();
	}
	const types::Type* TypeChecking::visit(const CallExp *e)
	{
		//syntax: fname(exp1, exp2, ..., expn)
		//check if fname is defined by looking up the symbol table
		string function = e->getFunc();
		//if fname is not defined, report an error, and return INT
		if (env.getVarEnv()->contains(function)) {
			error(e, "fname is undefined");
			return new types::INT();
		}
		else {
			//if fname is defined, get its data type, say t
			const types::Type *function_type = env.getVarEnv()->lookup(function).info->actual();
			//if t is not FUNCTION, report an error and return INT;
			if (dynamic_cast<const types::FUNCTION *>(function_type) != NULL) {
				error(e, "fname must be defined as Function type");
				return new types::INT();
			}
			else {
				//Let c_arg refers to the first argument (argument list can be found in CallExp)
				const ExpList* call_args = e->getArgs();
				//Let c_par refers to the first parameter (parameter list can be found in FUNCTION)
				vector <const types::Type*> args_vector = dynamic_cast <const types::FUNCTION*>(env.getVarEnv()->lookup(e->getFunc()).info)->getFieldType();
				vector <const types::Type*>::iterator args_iterator = args_vector.begin();
				vector <const types::Type*>::iterator args_end = args_vector.end();
				//repeat as long as both c_arg and c_par are not NULL
				while (call_args != NULL && args_iterator != args_end) {
					//perform type checking on c_arg and get its type, see ta
					const types::Type* arg_type = visit(call_args->getHead());
					//if ( ta is not compatible with type of c_par )
					if (!arg_type->coerceTo(*args_iterator)) {
						//report an error
						error(e, "argument type is not compatible with type provided in a function definition");
					}
					//update c_arg to refer to next argument
					args_iterator++;
					//update c_par to refer to next parameter
					call_args = call_args->getRest();
				}
				//if (c_arg is not null && c_par is null)
				if (call_args != NULL && args_iterator != args_end) {
					error(e, "Too many arguments provided");
				}
				//if (c_arg is null && c_par is not null )
				if (call_args == NULL && args_iterator != args_end) {
					error(e, "Too few arguments provided");
				}
				//return the result type of the function (can be found in t)
				return function_type;
			}
		}
	}
	/*	const types::Type* TypeChecking::visit(const RecordExp *e)
	{
	//add your implementation here
	//syntax: record_type {f1=exp1, ..., fn=expn}
	}
	*/
	const types::Type* TypeChecking::visit(const SeqExp *e)
	{
		//syntax: exp1; exp2; exp3; ....; expn
		const absyn::ExpList* exp_list = e->getList();
		const absyn::Exp* current = exp_list->getHead();
		const types::Type* current_type;
		//for each expression exp_i in the list
		while (exp_list != NULL) {
			//perform type checking on exp_i and save its data type to t
			current_type = visit(current);
			exp_list = exp_list->getRest();
		}
		//return t;
		return current_type;
	}
	const types::Type* TypeChecking::visit(const AssignExp *e)
	{
		//syntax: lvalue := exp
		//perform type checking on lvalue and save its data type to t
		const types::Type* lvalue_type = visit(e->getVar());
		//perform type checking on exp and save its data type to te
		const types::Type* exp_type = visit(e->getExp());
		//if ( te is NOT compatible with t )
		if (!exp_type->coerceTo(lvalue_type) || !lvalue_type->coerceTo(exp_type)) {
			// report an error
			error(e, "Variable type is incompatible with the provided expression.");
		}
		//return VOID
		return new types::VOID();
	}
	const types::Type* TypeChecking::visit(const IfExp *e)
	{
		//syntax: if test then
		//				exp1
		//			else
		//				exp2
		//1.	perform type checking on test and save its data type to t
		const types::Type* test_type = visit(e->getTest());
		//2.	if t is not INT, report an error
		if (dynamic_cast<const types::INT *>(test_type) != NULL) {
			error(e, "Test type has to be of a type INT");
		}
		//3.	perform type checking on exp1 and save its data type to t1
		const types::Type* then_type = visit(e->getThenClause());
		//4.	if it is a if-then satement (no else-clause)
		if (e->getElseClause() == NULL) {
			//if t1 is not VOID, report an error
			if (dynamic_cast<const types::VOID *>(then_type) != NULL) {
				error(e, "Then clause should be of a type VOID");
			}
			//return VOID;
			return new types::VOID();
			//5.	else (if-then-else expression)
		}
		else {
			//perform type checking on exp2 and save its data type to t2
			const types::Type* else_type = visit(e->getElseClause());
			//if t1 is compatible with t2
			if (then_type->coerceTo(else_type)) {
				//return t2
				return else_type;
				//else if t2 is compatible with t1
			}
			else if (else_type->coerceTo(then_type)) {
				//return t1
				return then_type;
				//else
			}
			else {
				//report an error;
				error(e, "then expression type and else expression type are incompatible");
				//return t1
				return then_type;
			}
		}
	}
	const types::Type* TypeChecking::visit(const WhileExp *e)
	{
		//syntax: while test do exp1
		//1.	perform type checking on test and save its data type to t
		const types::Type* test_type = visit(e->getTest());
		//2.	if t is not INT, report an error
		if (dynamic_cast<const types::INT *>(test_type) != NULL) {
			error(e, "test exppression type has to be INT");
		}
		//3.	perform type checking on exp1 and save its data type to t1
		const types::Type* body_type = visit(e->getBody());
		//4.	if t1 is not VOID, report an error
		if (dynamic_cast<const types::VOID *>(body_type) != NULL) {
			error(e, "body expression has to be type VOID");
		}
		//5.	return VOID;
		return new types::VOID();
	}
	const types::Type* TypeChecking::visit(const ForExp *e)
	{
		//syntax: for vname := exp1 to exp2 do exp3
		//Create a new scope for var/function symbol table
		env.getVarEnv()->beginScope();
		//Perform type checking on (vname := exp1), which is treated as a variable declaration
		const types::Type* var_dec_type = visit(e->getVar());
		if (dynamic_cast<const types::INT *>(var_dec_type) != NULL) {
			error(e, "Variable must be of a type INT");
		}
		//Perform type checking on exp2, and save its data type to t2
		const types::Type* t2 = visit(e->getHi());
		//if t2 is not INT, report an error
		if (dynamic_cast<const types::INT *>(t2) != NULL) {
			error(e, "High variable must be of a type INT");
		}
		//Perform type checking on exp3, and save its data type to t3
		const types::Type* t3 = visit(e->getBody());
		//if t3 is not VOID, report an error
		if (dynamic_cast<const types::VOID *>(t3) != NULL) {
			error(e, "For loop body must have type VOID");
		}
		//end the scope of var/function symbol table
		env.getVarEnv()->endScope();
		//return VOID
		return new types::VOID();
	}
	const types::Type* TypeChecking::visit(const BreakExp *e)
	{
		//return VOID if  you don't want bonus points.
		return new types::VOID();
	}
	const types::Type* TypeChecking::visit(const LetExp *e)
	{
		//syntax: let decls in exps end
		//Create a new scope for var/function symbol table
		env.getVarEnv()->beginScope();
		//Create a new scope for type symbol table
		env.getTypeEnv()->beginScope();
		const DecList* decs = e->getDecs();
		const Dec* first_dec = decs->getHead();
		//for each decl in the declaration list
		while (decs != NULL) {
			//perform type checking on the decl
			visit(first_dec);
			decs = decs->getRest();
			first_dec = decs->getHead();
		}
		//Perform type checking on exps and save its data type to t
		const types::Type* t = visit(e->getBody());
		//if t is an VOID, report an error (???)
		if (dynamic_cast<const types::VOID *>(t) == NULL) {
			error(e, "Let expression body can't return VOID type");
		}
		//end the scope of var/function symbol table
		env.getVarEnv()->endScope();
		env.getTypeEnv()->endScope();
		//return t;
		return t;
	}
	const types::Type* TypeChecking::visit(const ArrayExp *e)
	{
		//syntax: array_type [exp1] of exp2
		//1.	if array_type exists.
		if (!env.getTypeEnv()->contains(e->getType())) {
			//If it doesn't exist, report an error;
			error(e, "Type of array_type does not exist");
			//Let t be ARRAY of INT
			return new types::ARRAY(new types::INT());
		//2.	else
		}
		else {
			//lookup type symbol table to find and save its type to t;
			const types::Type* t = env.getTypeEnv()->lookup(e->getType()).info->actual();
			//if t is not ARRAY,
			if (dynamic_cast<const types::ARRAY *>(t) == NULL) {
				//report an error;
				error(e, "Type of array_type has to be ARRAY");
				//Let t be ARRAY of INT
				return new types::ARRAY(new types::INT());
			}
			//3.	perform type checking on exp1 and save its type to t1
			const types::Type* t1 = visit(e->getSize());
			//4.	if t1 is not INT, report an error
			if (dynamic_cast<const types::INT *>(t) == NULL) {
				error(e, "Type of exp1 has to be INT");
			}
			//5.	perform type checking on exp2 and save its type to t2
			const types::Type* t2 = visit(e->getInit());
			//6.	if t2 is not compatible to ((ARRAY *)t)->getElement();
			if (!t2->coerceTo(((types::ARRAY *)t)->getElement())) {
				//report an error
				error(e, "Type of exp2 is not compatible with array element type");
			}
			//7.	return t;
			return t;
		}
		return new types::STRING();
	}
	/*	const types::Type* TypeChecking::visit(const FunctionDec *d)
	{
	//add your implementation
	//syntax: function fname(p1:type1, ..., pn:typen) : rtype = exp1
	}
	*/
	const types::Type* TypeChecking::visit(const VarDec *d)
	{
		// syntax: var vname : Type = exp1
		//1.	if vname is defined locally  (use localContains function)
		if (env.getVarEnv()->localContains(d->getName())) {
			//report an error
			error(d, "vname cant be defined locally");
		}
		//2.	if Type is provided
		if (d->getType()) {
			//if Type doesn't exist in type symbol table
			if (!env.getTypeEnv()->contains(d->getType()->getName())) {
				//report an error
				error(d, "Type doesnt exist in type symbol table");
				//else
			}
			else {
				//lookup type symbol table to find and save its type information to tt;
				const types::Type *tt = env.getTypeEnv()->lookup(d->getType()->getName()).info;
				//Perform type checking on exp1 and save its type to t1
				const types::Type* t1 = visit(d->getInit());
				//if t1 is not compatible with tt
				if (!t1->coerceTo(tt)) {
					//report an error
					error(d, "type and initialization expression are not compatible");
				}
				//insert vname into the var/function symbol table
				insertVar(d->getName(), SymTabEntry(env.getVarEnv()->getLevel(), (types::Type*)t1, d));
			}
			//3.	else (Type is not provided)
		}
		else {
			//Perform type checking on exp1 and save its type to t1
			const types::Type* t1 = visit(d->getInit());
			//insert vname into the var/function symbol table
			insertVar(d->getName(), SymTabEntry(env.getVarEnv()->getLevel(), (types::Type*)t1, d));
		}
		//4.	return NULL;
		return NULL;
	}
	const types::Type* TypeChecking::visit(const TypeDec *d)
	{
		const types::Type*		type;
		types::NAME*			name = new types::NAME(d->getName());
		//find type redefine in the consecutive type declarations
		const absyn::TypeDec*	td = d->getNext();
		while (td != NULL) {
			if (td->getName() == d->getName())
				error(td, "type redefined");
			td = td->getNext();
		}
		name->bind(new types::INT());	//just for avoiding the self loop, later it
										//will be replaced by actual value
		insertType(d->getName(), SymTabEntry(env.getVarEnv()->getLevel(), name, d));
		if (d->getNext() != NULL)
			visit(d->getNext());
		type = visit(d->getTy());
		name->bind((types::Type *)type);
		env.getTypeEnv()->lookup(d->getName()) = SymTabEntry(env.getVarEnv()->getLevel(),
			name,
			d);
		if (name->isLoop()) {
			error(d, "illegal cycle found in type definition");
			name->bind(new types::INT());
		}
		return NULL;
	}
	const types::Type* TypeChecking::visit(const NameTy *t)
	{
		if (!(env.getTypeEnv()->contains(t->getName())))
		{
			error(t, "undefined type name");
			return new types::INT();
		}
		return env.getTypeEnv()->lookup(t->getName()).info;
	}
	/*	const types::Type* TypeChecking::visit(const RecordTy *t)
	{
	const absyn::FieldList*		fl = t->getFields();
	if ( fl == NULL ) {
	//empty record
	return new types::RECORD( "", NULL, NULL );
	}
	else {
	types::RECORD		*r = NULL, *tail = NULL, *head = NULL;
	while ( fl != NULL ) {
	if ( !env.getTypeEnv()->contains(fl->getType()) )
	r = new types::RECORD(	fl->getName(),
	new types::INT(),
	NULL );
	else
	r = new types::RECORD(	fl->getName(),
	env.getTypeEnv()->lookup(fl->getType()).info,
	NULL );
	if ( head == NULL )
	head = tail = r;
	else {
	tail->setRest(r);
	tail = r;
	}
	fl = fl->getRest();
	}
	return head;
	}
	}
	*/
	const types::Type* TypeChecking::visit(const ArrayTy *t)
	{
		if (!(env.getTypeEnv()->contains(t->getName())))
		{
			error(t, "undefined type");
			return new types::INT();
		}
		return new types::ARRAY(env.getTypeEnv()->lookup(t->getName()).info);
	}
} // end of namespace semantics
  /*
  Bonus points:
  1. Break expression
  algorithm:
  1. create a symbol table say lv (lv is actually a member data of class TypeChecking;
  2. everytime entering a loop:
  create a new scope for lv,
  insert breakSign into lv, its data type is INT
  3. everytime exiting a loop;
  destroy the scope for lv
  4. everytime entering a function declaration
  create a new scope for lv,
  insert breakSign into lv, its data type is VOID
  5. everytime exiting a function;
  destroy the scope for lv
  6. in visit(BreakExp)
  lookup lv symbol table to find the definition of breakSign
  and get its data type t
  if t is VOID, report an error
  2. No modification to loop variable
  algorithm:
  1. Everytime entering a for loop
  create a new scope for lv
  insert the loop variable into lv
  2. Every time leaving a for loop
  destroy the scope for lv
  3. In visit(AssignExp)
  if Var in the assignment expression is a simple var
  retrieve information (say v1) of var from the var/function symbol table
  retrieve information (say v2) of var from the lv symbol table
  if v1 and v2 points to the same node in the AbstractSyntaxTree,
  report an error
  */